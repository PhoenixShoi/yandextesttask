package com.phoenixshoi.yandextest;

import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ThreadJSONClass{

    private OnCompliteLoad listener;
    private String jsonLink;

    public interface OnCompliteLoad {
        void compliteLoad(String result);
    }

    public ThreadJSONClass(OnCompliteLoad listener, String jsonLink){
        this.listener = listener;
        this.jsonLink = jsonLink;
        new TaskParseYandex().execute();
    }

    private void callListeners(String result){
        listener.compliteLoad(result);
    }

    class TaskParseYandex  extends AsyncTask<Void, Void, String> {

        private HttpURLConnection urlConnection = null;
        private BufferedReader reader = null;
        private String resultJson = "";

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL(jsonLink);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return  resultJson;
        }

        @Override
        protected void onPostExecute(String result) {
            callListeners(result);
            super.onPostExecute(result);
        }
    }
}