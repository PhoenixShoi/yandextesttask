package com.phoenixshoi.yandextest;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class FragmentListClass extends Fragment{

    private ListView listViewCategory;
    private ArrayList<String> listItems;
    private ArrayAdapter<String> listAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.list_fragment, container, false);

        listViewCategory = (ListView) rootView.findViewById(R.id.listViewCategory);

        listItems = new ArrayList<String>();
        listAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_list_item_1, listItems);

        listViewCategory.setAdapter(listAdapter);

        if(getArguments() != null){
            for(String str : getArguments().getStringArray("Strings")){
                listItems.add(str);
            }
            listAdapter.notifyDataSetChanged();
        }

        listViewCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                OnItemSelectList listener = (OnItemSelectList) getActivity();
                listener.onItemSelect((int) id);
            }
        });

        return rootView;
    }

    public interface OnItemSelectList{
        void onItemSelect(int idItem);
    }
}