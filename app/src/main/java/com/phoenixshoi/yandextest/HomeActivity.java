package com.phoenixshoi.yandextest;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;



public class HomeActivity extends Activity implements FragmentListClass.OnItemSelectList , ThreadJSONClass.OnCompliteLoad{

    FragmentManager fragmentManager;

    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        fragmentManager = getFragmentManager();

        String[] catsName = {
                "1",
                "9",
                "9",
                "7",
                "0",
                "3",
                "2",
                "8"
        };

        this.resetFragment(catsName);
    }

    void resetFragment(String[] strings){
        fragmentTransaction = fragmentManager.beginTransaction();

        Bundle bundle = new Bundle();

        bundle.putStringArray("Strings", strings);

        FragmentListClass newListFragment = new FragmentListClass();

        newListFragment.setArguments(bundle);

        fragmentTransaction.add(R.id.homeLayout,newListFragment);

        fragmentTransaction.commit();
    }

    @Override
    public void onItemSelect(int idItem) {
        Toast.makeText(this, String.valueOf(idItem), Toast.LENGTH_SHORT).show();
        ThreadJSONClass myThread = new ThreadJSONClass(this,getResources().getString(R.string.json_link));
    }

    @Override
    public void compliteLoad(String result) {
        Log.i("", "compliteLoad" + result);
    }
        /*
        private void parseJSONObjects(String newJsonString) {
            try {
                JSONArray jsonArray = new JSONArray(newJsonString);

                for (int i = 0; i < savePath.size(); i++) {
                    JSONObject category = jsonArray.getJSONObject(savePath.get(i));
                    if (!category.isNull("subs")) {
                        jsonArray = new JSONArray(category.getString("subs"));
                    } else {
                        if (!category.isNull("id")) {
                            Toast.makeText(getApplicationContext(), "Element ID:" + category.getString("id"), Toast.LENGTH_SHORT).show();
                            savePath.remove(savePath.size() - 1);
                        }
                    }
                }

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject category = jsonArray.getJSONObject(i);
                    if (!category.isNull("title")) {
                        listItems.add(category.getString("title"));
                        listAdapter.notifyDataSetChanged();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        void updateList() {
            listItems.clear();
            listAdapter.notifyDataSetChanged();
            this.parseJSONObjects(this.resultJson);
        }
        */
}

